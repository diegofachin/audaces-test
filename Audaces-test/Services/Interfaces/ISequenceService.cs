﻿using Audaces_test.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Audaces_test.Services.Interfaces
{
    public interface ISequenceService
    {
        Task<ResponseSequenceDto> ValidatorSequence(RequestSequenceDto requestSequenceDto);
        ResponseHistoricDto HistoricSequence(DateTime StartDate, DateTime EndDate);
    }
}

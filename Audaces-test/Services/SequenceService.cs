﻿using Audaces.Domain.Repository.Generic;
using Audaces_test.Domain.Entity;
using Audaces_test.Dto;
using Audaces_test.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Audaces_test.Services
{
    public class SequenceService : ISequenceService
    {
        private List<int> NewSequence = new() { };

        private readonly IRepository<Historic> _historicRepository;

        public SequenceService(IRepository<Historic> historicRepository)
        {
            _historicRepository = historicRepository;
        }

        public async Task<ResponseSequenceDto> ValidatorSequence(RequestSequenceDto requestSequenceDto)
        {
            NewSequence = new() { };

            var sequenceOrderBy = requestSequenceDto.Sequence.OrderByDescending(x => ((uint)x));
            SumSequence(sequenceOrderBy.ToList(), requestSequenceDto.Target, new List<int>());

            await CreateHistoric(sequenceOrderBy.ToList(), NewSequence, requestSequenceDto.Target);

            ResponseSequenceDto responseSequenceDto = new()
            {
                Sequence = NewSequence 
            };

            return responseSequenceDto;
        }

        private void SumSequence(List<int> sequence, int target, List<int> partial)
        {
            int s = 0;
            foreach (int x in partial) s += x;

            if (s == target)
            {
                NewSequence = partial;
                return;
            }

            if (s >= target)
                return;

            for (int i = 0; i < sequence.Count; i++)
            {
                List<int> remaining = new();
                int n = sequence[i];
                for (int j = i + 1; j < sequence.Count; j++) remaining.Add(sequence[j]);

                List<int> partcialRec = new(partial);
                partcialRec.Add(n);
                SumSequence(remaining, target, partcialRec);
            }
        }

        private async Task<Historic> CreateHistoric(List<int> sequenceIn, List<int> sequenceOut, int target)
        {
            Historic historic = new()
            {
                SequenceIn = string.Join(",", sequenceIn),
                SequenceOut = string.Join(",", sequenceOut),
                Target = target,
                Date = DateTime.Now,

            };

            return await _historicRepository.Create(historic);
        }

        public ResponseHistoricDto HistoricSequence(DateTime StartDate, DateTime EndDate)
        {
            var historic = _historicRepository.FindAll().GetAwaiter().GetResult();
            var filter = historic.Where(h => h.Date.Date >= StartDate & h.Date.Date <= EndDate);

            var responseHistoricDto = new ResponseHistoricDto();
            HistoricItemDto historicItem = new();
            responseHistoricDto.LogItems = new();

            foreach (var item in filter)
            {
                historicItem = new();
                historicItem.Id = (int)item.Id;
                historicItem.SequenceIn = item.SequenceIn;
                historicItem.SequenceOut = item.SequenceOut;
                historicItem.Target = item.Target;
                historicItem.Date = item.Date;

                responseHistoricDto.LogItems.Add(historicItem);
            }

            return responseHistoricDto;
        }
    }
}

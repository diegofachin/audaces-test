﻿using Audaces_test.Domain.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Audaces.Domain.Repository.Generic
{
    public interface IRepository<T> where T : BaseEntity
    {
        Task<T> Create(T item);
        Task<List<T>> FindAll();
    }
}

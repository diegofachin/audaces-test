﻿using Audaces.Domain.Repository.Generic;
using Audaces_test.Domain.Base;
using Audaces_test.Domain.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Audaces_test.Domain.Repository.Generic
{
    public class GenericRepository<T> : IRepository<T> where T : BaseEntity
    {
        protected MySQLContext _context;

        private readonly DbSet<T> dataset;

        public GenericRepository(MySQLContext context)
        {
            _context = context;
            dataset = _context.Set<T>();
        }

        public async Task<List<T>> FindAll()
        {
            return await dataset.ToListAsync();
        }

        public async Task<T> Create(T item)
        {
            try
            {
                dataset.Add(item);
                await _context.SaveChangesAsync ();
                return item;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}

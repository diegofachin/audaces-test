﻿using Audaces_test.Domain.Entity;
using Microsoft.EntityFrameworkCore;

namespace Audaces_test.Domain.Context
{
    public class MySQLContext : DbContext
    {
        public MySQLContext()
        {

        }

        public MySQLContext(DbContextOptions<MySQLContext> options) : base(options)
        {

        }

        public DbSet<Historic> Historic { get; set;}
    }
}

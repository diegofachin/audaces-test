﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Audaces_test.Domain.Base
{
    public class BaseEntity
    {
        [Column("id")]
        public long Id { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Audaces_test.Dto
{
    public class HistoricItemDto
    {
        public int Id { get; set; }
        public string SequenceIn { get; set; }

        public string SequenceOut { get; set; }

        public int Target { get; set; }

        public DateTime Date { get; set; }
    }
}

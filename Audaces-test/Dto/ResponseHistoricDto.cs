﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Audaces_test.Dto
{
    public class ResponseHistoricDto
    {
        public List<HistoricItemDto> LogItems { get; set; }
    }
}

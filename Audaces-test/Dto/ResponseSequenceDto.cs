﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Audaces_test.Dto
{
    public class ResponseSequenceDto
    {
        public List<int> Sequence { get; set; }
    }
}

﻿using Audaces_test.Dto;
using Audaces_test.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Audaces_test.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SequenceController : ControllerBase
    {

        private readonly ISequenceService _sequenceService;

        public SequenceController(ISequenceService sequenceService)
        {
            _sequenceService = sequenceService;
        }

        [HttpPost("Validator")]
        [ProducesResponseType(typeof(RequestSequenceDto), StatusCodes.Status200OK)]
        [ProducesResponseType(400)]
        public async Task<ActionResult<ResponseSequenceDto>> ValidatorSequence([FromBody] RequestSequenceDto requestSequenceDto)
        {
            return Ok(await _sequenceService.ValidatorSequence(requestSequenceDto));
        }
        
        [HttpGet("Historic")]
        [ProducesResponseType((200), Type = typeof(ResponseHistoricDto))]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public ActionResult<ResponseHistoricDto> HistoricSequence([FromQuery] DateTime StartDate, [FromQuery] DateTime EndDate)
        {
            return Ok(_sequenceService.HistoricSequence(StartDate, EndDate));
        }
        
    }
}
